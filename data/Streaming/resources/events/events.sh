#!/usr/bin/env bash

rm -rf resources/streaming/*.log

for i in 1 2
do
	for i in resources/events/*.log
	do
		sleep $(($RANDOM % 2 ))
		cp $i resources/streaming/
	done
done
