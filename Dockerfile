FROM jupyter/all-spark-notebook

USER root

# Spark GraphFrames download
RUN cd /usr/local
RUN wget https://repos.spark-packages.org/graphframes/graphframes/0.8.0-spark3.0-s_2.12/graphframes-0.8.0-spark3.0-s_2.12.jar && \
    unzip graphframes-0.8.0-spark3.0-s_2.12.jar && \
    mv graphframes-0.8.0-spark3.0-s_2.12.jar $SPARK_HOME/jars/

ENV PYTHONPATH $PYTHONPATH:/usr/local/graphframes
