# Spark

## Prerequisites

- Install [Docker (Desktop)](https://docs.docker.com/get-docker/) and [Docker Compose](https://docs.docker.com/compose/install/) depending on your OS
-> Ubuntu Users install Docker Compose following the official Docker Guide and do not use the snap package manager

- [Clone](https://git-scm.com/docs/git-clone) or Download this repository

## Troubleshooting

- Be aware of the [Docker Manual for Windows](https://docs.docker.com/docker-for-windows/), be sure to pay attention to the information about file sharing and follow the steps for this repository
- When using Docker Hub make sure to create a Docker ID and sign in
- Working with Windows be aware of possible Firewall restrictions


## Start the Container
- Make sure Docker is running
- Navigate into the folder you saved the repository in and execute the command
    ```
    $ docker-compose up -d
    ```
- When the pulling process is complete the Container will be started 
 and you can access it in your browser on port 8888
    ```
    localhost:8080
    ````
- The password is ```ApacheSpark```

## Work with Jupyter Lab

- For basic information and help on how to work with Jupyter Lab look [here](https://docs.jupyter.org/en/latest/start/index.html) and [here](https://towardsdatascience.com/jypyter-notebook-shortcuts-bf0101a98330)




